var TGame = function()
{
    var Settings = {
        GRID: true,
        GRID_SIZE_WIDTH:   18,
        GRID_SIZE_HEIGHT:  11,
        GRID_CELL_SIZE:    25,
        EMPTY_CELLS:       0
    };

    var canvas;
    var videoCanvas;
    var video;
    var ctx;
    var videoCtx;
    var canvasWidth;
    var canvasHeight;

    var map         = [
        { pointY: 0, pointX: 0, sliceX: 0, sliceY: 0, empty: false }
    ];

    this.init = function()
    {
        // prepare elements
        canvas          = document.getElementById("canvas");
        videoCanvas     = document.createElement("canvas");
        video           = document.getElementById("video");

        canvasWidth     = Settings.GRID_SIZE_WIDTH  * Settings.GRID_CELL_SIZE;
        canvasHeight    = Settings.GRID_SIZE_HEIGHT * Settings.GRID_CELL_SIZE;

        ctx             = canvas.getContext("2d");
        videoCtx        = videoCanvas.getContext("2d");

        videoCanvas.setAttribute("width",  canvasWidth);
        videoCanvas.setAttribute("height",  canvasHeight);

        canvas.setAttribute("width",  canvasWidth);
        canvas.setAttribute("height", canvasHeight);

        // show grid
        drawGrid();

        // generate random map
        generateMap();

        //randomize map
        shuffleMap();

        // drawPuzzle when data is loaded.
        video.addEventListener("seeked", function() {
            drawVideo();
        }, false);

        var interval;
        video.addEventListener("loadeddata", function() {
            if ( ! interval )
            {
                interval = setInterval(drawVideo, 60);
            }
        }, false);

        video.addEventListener("play", function() {
            if ( ! interval )
            {
                interval = setInterval(drawVideo, 60);
            }
        }, false);

        video.addEventListener("playing", function() {
            if ( ! interval )
            {
                interval = setInterval(drawVideo, 60);
            }
        }, false);

        video.addEventListener("pause", function() {
            clearInterval(interval);
            interval = false;
        }, false);

        video.addEventListener("waiting", function() {
            clearInterval(interval);
            interval = false;
        }, false);
    };

    var shuffleMap = function()
    {
        var tmp = clone(map);
        tmp.sort(function() {
            return 0.5 - Math.random()
        });

        for ( var i = 0, j = map.length; i < j; i++ )
        {
            map[i].sliceX = clone(tmp[i].sliceX);
            map[i].sliceY = clone(tmp[i].sliceY);
        }
    };

    var generateMap = function ()
    {
        map = [];

        var prevRow = 0;
        var pointX  = 0;
        var pointY  = 0;

        for ( var i = 0, j = (Settings.GRID_SIZE_WIDTH * Settings.GRID_SIZE_HEIGHT); i < j; i++ )
        {
            var row = Math.ceil((i+1) / Settings.GRID_SIZE_WIDTH);

            if ( prevRow != row && i > 0 )
            {
                pointX = 0;
                pointY += Settings.GRID_CELL_SIZE;
            }

            var cell = {
                pointX: pointX,
                pointY: pointY,
                sliceX: pointX,
                sliceY: pointY,
                empty:  false
            };

            if( i+Settings.EMPTY_CELLS >= j )
            {
                cell.empty = true;
            }

            prevRow = row;
            pointX += Settings.GRID_CELL_SIZE;

            map.push(cell);
        }
    };

    function clone(obj){
        if(obj == null || typeof(obj) != 'object')
            return obj;

        var temp = obj.constructor(); // changed

        for(var key in obj)
            temp[key] = clone(obj[key]);
        return temp;
    }

    var drawGrid = function()
    {
        ctx.clearRect(0, 0, canvasWidth, canvasHeight);

        // draw grid
        if ( Settings.GRID )
        {
            for( var y = 0; y < Settings.GRID_SIZE_WIDTH-1; y++ )
            {
                ctx.moveTo((y * Settings.GRID_CELL_SIZE) + Settings.GRID_CELL_SIZE, 0);
                ctx.lineTo((y * Settings.GRID_CELL_SIZE) + Settings.GRID_CELL_SIZE, canvasHeight);
                ctx.strokeStyle = "#f2f0eb";
                ctx.stroke();
            }

            for( var x = 0; x < Settings.GRID_SIZE_HEIGHT-1; x++ )
            {
                ctx.moveTo(0, (x * Settings.GRID_CELL_SIZE) + Settings.GRID_CELL_SIZE);
                ctx.lineTo(canvasWidth, (x * Settings.GRID_CELL_SIZE) + Settings.GRID_CELL_SIZE);
                ctx.strokeStyle = "#f2f0eb";
                ctx.stroke();
            }
        }

    };

    var drawVideo = function()
    {
        videoCtx.clearRect(0, 0, canvasWidth, canvasHeight);
        videoCtx.drawImage(video, 0, 0, canvasWidth, canvasHeight);

        for( var i = 0; i < map.length; i++ )
        {
            if( map[i] != null && ! map[i].empty )
            {
                ctx.drawImage(videoCanvas,
                    map[i].sliceX, map[i].sliceY,
                    Settings.GRID_CELL_SIZE-1, Settings.GRID_CELL_SIZE-1,
                    map[i].pointX, map[i].pointY,
                    Settings.GRID_CELL_SIZE-1, Settings.GRID_CELL_SIZE-1
                );
            }
            else
            {
                ctx.fillRect(map[i].sliceX, map[i].sliceY, Settings.GRID_CELL_SIZE-1, Settings.GRID_CELL_SIZE-1);
            }
        }
    };
};

