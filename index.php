<doctype html>
<html>
    <head>
        <title>T canvas</title>
        <script type="text/javascript" src="game.js"></script>
    </head>
    <body>
        <canvas id="canvas" width="300" height="300" style="border: 1px black solid;"></canvas>
        <video id="video" width="480" height="267" durationHint="33" controls>
            <source src="http://download.blender.org/peach/bigbuckbunny_movies/BigBuckBunny_320x180.mp4" />
        </video>
        <script type="application/javascript">
            var game = new TGame();
            game.init();
        </script>
    </body>
</html>
